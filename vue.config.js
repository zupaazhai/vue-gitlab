var publicPath = '/'

if (process.env.ON_GITLAB) {
  publicPath = 'vue-gitlab' 
}

module.exports = {
    publicPath: publicPath
}